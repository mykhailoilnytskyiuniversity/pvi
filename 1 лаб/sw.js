const assets = [
    "/",
    "/index.html",
    "/css/style.css",
    "/css/reset.css",
    "/script.js",
    "/img/",
];

self.addEventListener("install", (installEvent) => {
    installEvent.waitUntil(
        caches.open("pwa-assets").then((cache) => {
            cache.addAll(assets);
        })
    );
});

self.addEventListener("fetch", (fetchEvent) => {
    fetchEvent.respondWith(
        caches.match(fetchEvent.request).then((res) => {
            return res || fetch(fetchEvent.request);
        })
    );
});
