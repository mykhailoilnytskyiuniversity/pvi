var addStudent = document.getElementById("add-student");

addStudent.addEventListener("click", openAddStudentWindow, false);

var modalEditButtons = document.getElementsByClassName("edit");

var editModal = document.getElementById("edit-modal");

var delModal = document.getElementById("delete-modal");

var modalDelButtons = document.getElementsByClassName("delete");

var delRow;

var delButtonApprove = document.querySelectorAll(".delete-modal .approve");

var notificationBell = document.getElementById("bell");

var modalExitButtons = document.getElementsByClassName("cancel");

var closeButtons = document.getElementsByClassName("close");

if ("serviceWorker" in navigator) {
    self.addEventListener("load", async () => {
        const container = navigator.serviceWorker;
        if (container.controller === null) {
            const reg = await container.register("sw.js");
        }
    });
}

function MEGAFUNC() {
    Array.prototype.forEach.call(
        modalEditButtons,
        function addClickListener(btn) {
            btn.addEventListener("click", function (event) {
                editModal.style.display = "block";
                openEditStudentWindow(this.parentNode.parentNode.parentNode);
            });
        }
    );

    Array.prototype.forEach.call(
        modalDelButtons,
        function addClickListener(btn) {
            btn.addEventListener("click", function (event) {
                delModal.style.display = "block";

                delRow = btn.closest("tr");
            });
        }
    );

    Array.prototype.forEach.call(
        delButtonApprove,
        function addClickListener(btn) {
            btn.addEventListener("click", function (event) {
                delModal.style.display = "none";

                delRow.remove();
            });
        }
    );

    Array.prototype.forEach.call(
        modalExitButtons,
        function addClickListener(btn) {
            btn.addEventListener("click", function (event) {
                editModal.style.display = "none";
                delModal.style.display = "none";
            });
        }
    );

    Array.prototype.forEach.call(closeButtons, function addClickListener(btn) {
        btn.addEventListener("click", function (event) {
            editModal.style.display = "none";
            delModal.style.display = "none";
        });
    });

    window.onclick = function (event) {
        if (event.target == editModal || event.target == delModal) {
            editModal.style.display = "none";
            delModal.style.display = "none";
        }
    };

    //bell

    notificationBell.addEventListener("dblclick", bellClick, false);

    function bellClick() {
        var notifBadge = document.getElementById("notiffBell");
        notifBadge.style.visibility =
            notifBadge.style.visibility === "hidden" ? "visible" : "hidden";

        notificationBell.classList.remove("bell-animation");
        notificationBell.offsetWidth;
        notificationBell.classList.add("bell-animation");
    }
}

function addElement(pos = -1) {
    if (
        document.forms["studentForm"]["group"].value != "" &&
        document.forms["studentForm"]["first-name"].value != "" &&
        document.forms["studentForm"]["birthday"].value != "" &&
        document.forms["studentForm"]["gender"].value != ""
    ) {
        let table = document
            .getElementById("t")
            .getElementsByTagName("tbody")[0];
        let newRow = table.insertRow(3);

        let newCell1 = newRow.insertCell(0);
        var x = document.createElement("INPUT");
        x.setAttribute("type", "checkbox");
        newCell1.appendChild(x);

        let newCell2 = newRow.insertCell(1);
        let newCell3 = newRow.insertCell(2);
        let newCell4 = newRow.insertCell(3);
        let newCell5 = newRow.insertCell(4);

        newCell2.innerHTML = document.forms["studentForm"]["group"].value;
        newCell3.innerHTML = document.forms["studentForm"]["first-name"].value;
        newCell5.innerHTML = document.forms["studentForm"]["birthday"].value;
        newCell4.innerHTML = document.forms["studentForm"]["gender"].value;

        let newCell6 = newRow.insertCell(5);
        newCell6.innerHTML = `<div class="status">
                                    <div class="circle gray"></div>
                          </div>`;

        let newCell7 = newRow.insertCell(6);
        newCell7.innerHTML = `<div class="choose-items">
                                    <div class="delete">
                                        <button>
                                            <img
                                                src="./img/delete-button.svg"
                                                alt="delete"
                                            />
                                        </button>
                                    </div>
                                    <div class="edit">
                                        <button>
                                            <img
                                                src="./img/edit-button.svg"
                                                alt="edit"
                                            />
                                        </button>
                                    </div>
                          </div>`;
    }
    MEGAFUNC();
}

MEGAFUNC();

let editable;

function editStudent() {
    if (
        document.forms["studentForm"]["group"].value != "" &&
        document.forms["studentForm"]["first-name"].value != "" &&
        document.forms["studentForm"]["birthday"].value != "" &&
        document.forms["studentForm"]["gender"].value != ""
    ) {
        editable.innerHTML =
            '<tr><td><input type="checkbox"></td><td>' +
            document.forms["studentForm"]["group"].value +
            "</td><td>" +
            document.forms["studentForm"]["first-name"].value +
            "</td><td>" +
            document.forms["studentForm"]["gender"].value +
            "</td><td>" +
            document.forms["studentForm"]["birthday"].value +
            "</td><td>" +
            `<div class="status">
                                    <div class="circle gray"></div>
             </div>` +
            "</td><td>" +
            `<div class="choose-items">
                                    <div class="delete">
                                        <button>
                                            <img
                                                src="./img/delete-button.svg"
                                                alt="delete"
                                            />
                                        </button>
                                    </div>
                                    <div class="edit">
                                        <button>
                                            <img
                                                src="./img/edit-button.svg"
                                                alt="edit"
                                            />
                                        </button>
                                    </div>
                          </div>` +
            "</td></tr>";
    }
}

function openEditStudentWindow(el) {
    editable = el;

    document.querySelectorAll("input").forEach((input) => (input.value = ""));
    document
        .querySelectorAll("select")
        .forEach((select) => (select.value = ""));
    let butt = document.getElementById("b1");

    butt.onclick = editStudent;
}

function openAddStudentWindow() {
    editModal.style.display = "block";

    document.querySelectorAll("input").forEach((input) => (input.value = ""));
    document
        .querySelectorAll("select")
        .forEach((select) => (select.value = ""));

    let butt = document.getElementById("b1");
    butt.onclick = addElement;
}
